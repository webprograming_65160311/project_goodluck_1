import type { OrderBillDetail } from '@/types/OrderBillDetail'
import http from './http'

function getOrderBillDetails() {
  return http.get('/order-bill-detail')
}

function addOrderBillDetail(orderBillDetail: OrderBillDetail) {
  return http.post('/order-bill-detail', orderBillDetail)
}
export default {
  getOrderBillDetails,
  addOrderBillDetail
}
