import type { RawMaterial } from '@/types/RawMaterial'
import httpCheckStock from './httpCheckStock'
import http from './http'

function addRawMaterial(rawMaterial: RawMaterial & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', rawMaterial.name)
  // if (rawMaterial.quantity !== null) formData.append('quantity', rawMaterial.quantity.toString())
  // if (rawMaterial.price !== null) formData.append('price', rawMaterial.price.toString())
  // if (rawMaterial.minimum) formData.append('minimum', rawMaterial.minimum.toString())
  formData.append('unit', rawMaterial.unit)
  // if (rawMaterial.status) formData.append('status', rawMaterial.status)
  if (rawMaterial.files && rawMaterial.files.length > 0)
    formData.append('image', rawMaterial.files[0])
  return http.post('/raw-materials', formData, {
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

function updateRawMaterial(rawMaterial: RawMaterial & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', rawMaterial.name)
  // if (rawMaterial.quantity) formData.append('quantity', rawMaterial.quantity.toString())
  // if (rawMaterial.price) formData.append('price', rawMaterial.price.toString())
  // if (rawMaterial.minimum) formData.append('minimum', rawMaterial.minimum.toString())
  formData.append('unit', rawMaterial.unit)
  // if (rawMaterial.status) formData.append('status', rawMaterial.status)
  if (rawMaterial.files && rawMaterial.files.length > 0)
    formData.append('image', rawMaterial.files[0])
  return httpCheckStock.post(`/raw-materials/${rawMaterial.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delRawMaterial(rawMaterial: RawMaterial) {
  return http.delete(`/raw-materials/${rawMaterial.id}`)
}

function getRawMaterial(id: number) {
  return httpCheckStock.get(`/raw-materials/${id}`)
}

function getRawMaterials() {
  return httpCheckStock.get('/raw-materials')
}

function getLastId() {
  return http.get('/raw-materials/last-id')
}

export default {
  addRawMaterial,
  updateRawMaterial,
  delRawMaterial,
  getRawMaterial,
  getRawMaterials,
  getLastId
}
