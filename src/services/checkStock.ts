import type { CheckStock } from '@/types/CheckStock'
import http from './http'
import httpCheckStock from './httpCheckStock'

function getCheckStocks() {
  return httpCheckStock.get('/check-stock')
}

function addCheckStock(checkstock: CheckStock) {
  return httpCheckStock.post('/check-stock', checkstock)
}

function getLastId() {
  return httpCheckStock.get('/check-stock/last-id')
}
export default {
  getCheckStocks,
  addCheckStock,
  getLastId
}
