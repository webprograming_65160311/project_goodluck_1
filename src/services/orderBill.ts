import type { OrderBill } from '@/types/OrderBill'
import http from './http'

function getOrderBills() {
  return http.get('/order-bill')
}

function addOrderBill(orderBill: OrderBill) {
  return http.post('/order-bill', orderBill)
}

function getLastId() {
  return http.get('/order-bill/last-id')
}
export default {
  getOrderBills,
  addOrderBill,
  getLastId
}
