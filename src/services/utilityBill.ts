import type { UtilityBill } from '@/types/UtilityBill'
import http from './http'

function getUtilityBills() {
  return http.get('/utility-bill')
}

function addUtilityBill(orderBill: UtilityBill) {
  return http.post('/utility-bill', orderBill)
}

export default {
  addUtilityBill,
  getUtilityBills
}
