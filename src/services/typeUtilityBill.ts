import http from './http'
import type { TypeUtilityBill } from '@/types/TypeUtilityBill'

function addTypeUtilityBill(typeUtilityBill: TypeUtilityBill) {
  return http.post('/type-utility-bill', typeUtilityBill)
}

function getTypeUtilityBills() {
  return http.get('/type-utility-bill')
}
function getTypeUtilityBill(id: number) {
  return http.get(`/type-utility-bill/${id}`)
}

function removeTypeUtilityBill(typeUtilityBill: TypeUtilityBill) {
  return http.delete(`/type-utility-bill/${typeUtilityBill.id}`)
}

export default {
  addTypeUtilityBill,
  getTypeUtilityBills,
  getTypeUtilityBill,
  removeTypeUtilityBill
}
