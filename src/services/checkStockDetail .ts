import http from './http'
import type { CheckRawMaterialDetail } from '@/types/CheckRawMaterialDetail'
import httpCheckStock from './httpCheckStock'

function getCheckDetailStocks() {
  return httpCheckStock.get('/check-raw-material-detail')
}

function addCheckDetailStock(checkRawMaterialDetail: CheckRawMaterialDetail) {
  return httpCheckStock.post('/check-raw-material-detail', checkRawMaterialDetail)
}
export default {
  getCheckDetailStocks,
  addCheckDetailStock
}
