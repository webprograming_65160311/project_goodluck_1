import type { Stock } from '@/types/Stock'
import http from './http'

function getstocks() {
  return http.get('/stock')
}

function getstock(id: number) {
  return http.get(`/stock/${id}`)
}

function addStock(stock: Stock) {
  return http.post('/stock', stock)
}

function updateStock(stock: Stock) {
  return http.patch(`/stock/${stock.id}`, stock)
}

export default { getstocks, getstock, addStock, updateStock }
