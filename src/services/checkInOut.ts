import type { CheckIn } from '@/types/CheckIn'
import http from './http'

function checkIn(cIn: CheckIn) {
  //test
  cIn.checkIn = new Date(2024, 2, 1, 8, 0, 59)
  //checkIn.checkIn = new Date()
  const expectedCheckIn = new Date('2024-03-07T08:00:00')
  if (isLate(cIn.checkIn, expectedCheckIn)) {
    cIn.status = 'มาสาย'
  } else {
    cIn.status = 'ทันเวลา'
  }
  console.log('service check in', cIn)
  return http.post('/time-attendance', cIn)
}

function checkOut(cOut: CheckIn) {
  cOut.checkIn = new Date(cOut.checkIn)
  //test
  cOut.checkOut = new Date(2024, 2, 1, 16, 0, 59)
  //checkOut.checkOut = new Date()
  const diff = cOut.checkOut.getTime() - cOut.checkIn.getTime()
  cOut.hour = diff / (1000 * 60 * 60)
  console.log('service check out', cOut)
  return http.patch(`/time-attendance/${cOut.id}`, cOut)
}

function getCheckInOutByOne(id: number) {
  return http.get('/time-attendance/' + id)
}

function getCheckInOuts() {
  return http.get('/time-attendance')
}

function getCheckInOutMe(id: number) {
  return http.get(`/time-attendance/my-own/${id}`)
}

function getCheckInOutBranch(id: number) {
  return http.get(`/time-attendance/branch/${id}`)
}

function isLate(checkIn: Date, expectedCheckIn: Date): boolean {
  const gracePeriod = 15 * 60 * 1000 // Assume a 15-minute grace period in milliseconds
  // Calculate the latest allowed check-in time
  const latestCheckIn = new Date(expectedCheckIn.getTime() + gracePeriod)
  // Check if the actual check-in time is after the latest allowed check-in time
  return checkIn.getTime() > latestCheckIn.getTime()
}

export default {
  checkIn,
  checkOut,
  getCheckInOutByOne,
  getCheckInOuts,
  getCheckInOutMe,
  getCheckInOutBranch
}
