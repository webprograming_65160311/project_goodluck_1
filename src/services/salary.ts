import type { Salary } from '@/types/Salary'
import http from './http'

function getSalaries() {
  return http.get('/salary')
}

function getSalary(id: number) {
  return http.get(`/salary/${id}`)
}

function saveSlary(salary: Salary) {
  return http.post(`/salary`, salary)
}

export default { getSalaries, getSalary, saveSlary }
