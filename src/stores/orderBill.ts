import { ref } from 'vue'
import { defineStore } from 'pinia'
import orderBillService from '@/services/orderBill'
import type { OrderBill } from '@/types/OrderBill'
import { useUserStore } from './user'
import { useRawMaterialStore } from './rawMaterial'
import { useAuthStore } from './auth'

const lastId = ref()

export const useOrderBillStore = defineStore('orderBill', () => {
  const userStore = useUserStore()
  const rawMaterialStore = useRawMaterialStore()
  const authStore = useAuthStore()
  const orderBills = ref(<OrderBill[]>[])

  const initialOrderBill: OrderBill = {
    date: null,
    store: '',
    totalPrice: 0,
    user: userStore.initialUser
  }
  const editedOrderBill = ref<OrderBill>(JSON.parse(JSON.stringify(initialOrderBill)))
  const editedOrderBills = ref<OrderBill[]>([])

  async function getOrderBills() {
    try {
      const res = await orderBillService.getOrderBills()
      orderBills.value = res.data

      const select = orderBills.value.filter(
        (orderBill) => orderBill.user?.branch?.id === authStore.getCurrentUser()?.branch?.id
      )
      orderBills.value = select
    } catch (e: any) {
      console.log('error')
    }
  }

  async function saveOrderBill() {
    try {
      const orderBill = editedOrderBill.value
      console.log('id ' + orderBill.id)
      if (!orderBill.id) {
        console.log('new')
        //add new

        const res = await orderBillService.addOrderBill(orderBill)
      }
    } catch (e) {
      console.log('errer')
    }
  }

  function clearForm() {
    editedOrderBill.value = JSON.parse(JSON.stringify(initialOrderBill))
  }

  async function getLastId() {
    lastId.value = await orderBillService.getLastId()
    const lastIdResponse = await orderBillService.getLastId()
    lastId.value = lastIdResponse.data.lastId
  }

  async function calReceipt() {
    let totalBefore = 0
    for (const item of rawMaterialStore.rawMaterialItems) {
      if (item.stocks[0].price !== null && item.stocks[0].quantity !== null) {
        totalBefore += item.stocks[0].price * item.stocks[0].quantity
      }
    }
    editedOrderBill.value.totalPrice = totalBefore
  }
  function clearconsEteditedOrderBill() {
    editedOrderBill.value = JSON.parse(JSON.stringify(initialOrderBill))
  }

  return {
    getOrderBills,
    saveOrderBill,
    clearForm,
    getLastId,
    calReceipt,
    clearconsEteditedOrderBill,
    initialOrderBill,
    editedOrderBills,
    editedOrderBill,
    orderBills,
    lastId
  }
})
