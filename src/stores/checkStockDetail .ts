import { ref } from 'vue'
import { defineStore } from 'pinia'
import checkStockDetailService from '@/services/checkStockDetail '
import { useRawMaterialStore } from './rawMaterial'
import { useCheckStockStore } from './checkStock'
import type { CheckRawMaterialDetail } from '@/types/CheckRawMaterialDetail'

export const useCheckStockDetailStore = defineStore('checkStockDetail ', () => {
  const rawMaterialStore = useRawMaterialStore()
  const checkStockStore = useCheckStockStore()
  const checkStockDetails = ref(<CheckRawMaterialDetail[]>[])

  const initialCheckStockDetail: CheckRawMaterialDetail = {
    bfCheck: 0,
    atCheck: 0,
    rawMaterial: rawMaterialStore.initialRawMaterial,
    checkStock: checkStockStore.initialCheckStock
  }
  const editedCheckStockDetail = ref<CheckRawMaterialDetail>(
    JSON.parse(JSON.stringify(initialCheckStockDetail))
  )
  const editedCheckStockDetails = ref<CheckRawMaterialDetail[]>([])

  async function getCheckStocksDetails() {
    try {
      const res = await checkStockDetailService.getCheckDetailStocks()
      checkStockDetails.value = res.data
    } catch (e: any) {
      console.log('error')
    }
  }

  async function saveCheckStocksDetail() {
    try {
      const checkStockDetail = editedCheckStockDetail.value

      if (!checkStockDetail.id) {
        //add new

        const res = await checkStockDetailService.addCheckDetailStock(checkStockDetail)
      }
      await getCheckStocksDetails()
    } catch (e) {
      console.log(e)
    }
  }

  function clearForm() {
    editedCheckStockDetail.value = JSON.parse(JSON.stringify(initialCheckStockDetail))
  }

  return {
    getCheckStocksDetails,
    clearForm,
    saveCheckStocksDetail,
    editedCheckStockDetails,
    editedCheckStockDetail
  }
})
