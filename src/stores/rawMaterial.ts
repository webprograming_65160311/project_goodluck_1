import { ref } from 'vue'
import { defineStore } from 'pinia'
import rawMaterialService from '@/services/rawMaterial'
import { useOrderBillStore } from './orderBill'
import type { RawMaterial } from '@/types/RawMaterial'
import { useAuthStore } from './auth'
import type { Stock } from '@/types/Stock'
import stockService from '@/services/stock'

const lastId = ref()

export const useRawMaterialStore = defineStore('rawMaterial', () => {
  const orderBillStore = useOrderBillStore()
  const rawMaterials = ref(<RawMaterial[]>[])
  const authStore = useAuthStore()

  const initialRawMaterial: RawMaterial | Stock = {
    id: null,
    name: '',
    unit: '',
    image: 'noimage.jpg',
    files: [],
    stocks: [
      { id: null, price: null, quantity: null, minimum: null, branch: null, rawMaterial: null }
    ],
    branch: []
  }
  const initialRaw: RawMaterial = {
    id: null,
    name: '',
    unit: '',
    image: 'noimage.jpg',
    files: [],
    stocks: [],
    branch: []
  }

  const edited = ref<RawMaterial>(JSON.parse(JSON.stringify(initialRaw)))

  const editedRawMaterial = ref<RawMaterial & { files: File[] }>(
    JSON.parse(JSON.stringify(initialRawMaterial))
  )
  const editedIndex = -1
  const editedRawMaterials = ref<RawMaterial[]>([])

  const rawMaterialCopy = ref<RawMaterial[]>([])

  const rawMaterialItems = ref<RawMaterial[]>([])

  const editedRawMaterialCopy = ref<RawMaterial>(JSON.parse(JSON.stringify(initialRawMaterial)))

  function initialize() {
    rawMaterialCopy.value = rawMaterials.value.map((rawMaterial) => ({
      id: rawMaterial.id,
      name: rawMaterial.name,
      unit: rawMaterial.unit,
      files: [],
      stocks: [
        {
          id: rawMaterial.stocks[0].id,
          price: null,
          quantity: null,
          minimum: rawMaterial.stocks[0].minimum
        }
      ],
      branch: []
    }))
    // editedRawMaterialCopy.value = JSON.parse(JSON.stringify(rawMaterialCopy.value))
  }

  async function getLastId() {
    lastId.value = await rawMaterialService.getLastId()
    const lastIdResponse = await rawMaterialService.getLastId()
    lastId.value = lastIdResponse.data.lastId
  }

  const editedRawMaterialItem = ref<RawMaterial>(JSON.parse(JSON.stringify(rawMaterialItems)))

  function item() {
    editedRawMaterialItem.value = JSON.parse(JSON.stringify(rawMaterialItems))
  }

  function copyrawMaterials() {
    editedRawMaterials.value = JSON.parse(JSON.stringify(rawMaterials.value))
  }

  function calculateStatus(rawMaterial: RawMaterial): 'Normal' | 'Low Stock' | 'Out of Stock' {
    if (
      rawMaterial.stocks[0].quantity !== null &&
      rawMaterial.stocks[0].minimum !== null &&
      rawMaterial.stocks[0].minimum &&
      rawMaterial.stocks[0].quantity == 0
    ) {
      return (rawMaterial.stocks[0].status = 'Out of Stock')
    } else if (
      rawMaterial.stocks[0].quantity !== null &&
      rawMaterial.stocks[0].minimum !== null &&
      rawMaterial.stocks[0].minimum &&
      rawMaterial.stocks[0].quantity >= rawMaterial.stocks[0].minimum
    ) {
      return (rawMaterial.stocks[0].status = 'Normal')
    } else {
      return (rawMaterial.stocks[0].status = 'Low Stock')
    }
  }

  function addItem() {
    const existingItemIndex = rawMaterialItems.value.findIndex(
      (item) => item.id === editedRawMaterialCopy.value.id
    )
    if (editedRawMaterialCopy.value !== initialRawMaterial && existingItemIndex !== -1) {
      rawMaterialItems.value.splice(existingItemIndex, 1, { ...editedRawMaterialCopy.value })
      orderBillStore.calReceipt()
    } else if (editedRawMaterialCopy.value !== initialRawMaterial) {
      rawMaterialItems.value.push({ ...editedRawMaterialCopy.value })
      orderBillStore.calReceipt()
    }
  }
  function clear() {
    rawMaterialItems.value = []
  }

  async function getRawMaterial(id: number) {
    try {
      const res = await rawMaterialService.getRawMaterial(id)
      editedRawMaterial.value = res.data
    } catch (e: any) {
      console.log('error')
    }
  }

  async function getRaw(id: number) {
    try {
      const res = await rawMaterialService.getRawMaterial(id)
      edited.value = res.data
    } catch (e: any) {
      console.log('error')
    }
  }

  async function getRawMaterials() {
    try {
      const res = await rawMaterialService.getRawMaterials()
      rawMaterials.value = res.data

      const select = rawMaterials.value.filter(
        (rawMaterial) =>
          rawMaterial.stocks?.length &&
          rawMaterial.stocks[0].branch?.id === authStore.getCurrentUser()?.branch?.id
      )

      rawMaterials.value = select
    } catch (e: any) {
      console.log('error')
    }
  }

  async function saveRawMaterial() {
    try {
      const rawMaterial = editedRawMaterial.value
      console.log('id ' + rawMaterial.id)
      if (!rawMaterial.id) {
        //add new
        calculateStatus(rawMaterial)
        const res = await rawMaterialService.addRawMaterial(rawMaterial)
      } else {
        //update
        console.log('update')
        calculateStatus(rawMaterial)

        const res = await rawMaterialService.updateRawMaterial(rawMaterial)
      }
    } catch (e: any) {
      console.log('error')
    }
  }

  async function saveStock() {
    try {
      const rawMaterial = editedRawMaterial.value
      console.log('id ' + rawMaterial.stocks[0].id)
      if (!rawMaterial.stocks[0].id) {
        //add new
        console.log(rawMaterial.stocks[0])

        calculateStatus(rawMaterial)

        const res = await stockService.addStock(rawMaterial.stocks[0])
      } else {
        //update
        calculateStatus(rawMaterial)
        const res = await stockService.updateStock(rawMaterial.stocks[0])
        // console.log(res)
      }
    } catch (e: any) {
      console.log('error')
    }
  }

  async function delRawMaterial() {
    try {
      const rawMaterial = editedRawMaterial.value
      const res = await rawMaterialService.delRawMaterial(rawMaterial)
      await getRawMaterials()
    } catch (e: any) {
      console.log('error')
    }
  }

  function clearForm() {
    editedRawMaterial.value = JSON.parse(JSON.stringify(initialRawMaterial))
  }

  function clearRawMaterialCopy() {
    rawMaterialCopy.value = []
    editedRawMaterialCopy.value = JSON.parse(JSON.stringify(initialRawMaterial))
  }

  return {
    copyrawMaterials,
    addItem,
    initialize,
    getRawMaterials,
    getRawMaterial,
    saveRawMaterial,
    delRawMaterial,
    clearForm,
    clear,
    clearRawMaterialCopy,
    item,
    saveStock,
    getLastId,
    getRaw,
    editedRawMaterials,
    editedRawMaterialCopy,
    rawMaterialItems,
    rawMaterialCopy,
    editedRawMaterial,
    editedIndex,
    lastId,
    initialRawMaterial,
    rawMaterials,
    editedRawMaterialItem,
    edited
  }
})
