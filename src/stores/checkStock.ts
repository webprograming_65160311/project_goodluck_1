import { ref } from 'vue'
import { defineStore } from 'pinia'
import checkStockService from '@/services/checkStock'
import type { CheckStock } from '@/types/CheckStock'
import { useUserStore } from './user'
import { useAuthStore } from './auth'

const lastId = ref()

const authStore = useAuthStore()

export const useCheckStockStore = defineStore('checkStock', () => {
  const userStore = useUserStore()
  const checkStocks = ref(<CheckStock[]>[])

  const initialCheckStock: CheckStock = {
    checkDate: new Date(),
    user: userStore.initialUser
  }
  const editedCheckStock = ref<CheckStock>(JSON.parse(JSON.stringify(initialCheckStock)))
  const editedCheckStocks = ref<CheckStock[]>([])

  async function getCheckStocks() {
    try {
      const res = await checkStockService.getCheckStocks()
      checkStocks.value = res.data

      const select = checkStocks.value.filter(
        (checkStock) => checkStock.user?.branch?.id === authStore.getCurrentUser()?.branch?.id
      )
      checkStocks.value = select
    } catch (e: any) {
      console.log('error')
    }
  }

  async function saveCheckStock() {
    try {
      const checkStock = editedCheckStock.value
      console.log('id ' + checkStock.id)
      if (!checkStock.id) {
        console.log('new')
        //add new

        const res = await checkStockService.addCheckStock(checkStock)
      }
    } catch (e) {
      console.log('errer')
    }
  }

  function clearForm() {
    editedCheckStock.value = JSON.parse(JSON.stringify(initialCheckStock))
  }

  async function getLastId() {
    lastId.value = await checkStockService.getLastId()
    const lastIdResponse = await checkStockService.getLastId()
    lastId.value = lastIdResponse.data.lastId
  }

  return {
    getCheckStocks,
    saveCheckStock,
    clearForm,
    getLastId,
    initialCheckStock,
    editedCheckStocks,
    editedCheckStock,
    checkStocks,
    lastId
  }
})
