import { ref } from 'vue'
import { defineStore } from 'pinia'
import orderBillDetailService from '@/services/orderBillDetail'
import { useRawMaterialStore } from './rawMaterial'
import { useOrderBillStore } from './orderBill'
import type { CheckStock } from '@/types/CheckStock'
import type { OrderBillDetail } from '@/types/OrderBillDetail'

export const useOrderBillDetailStore = defineStore('orderBillDetail ', () => {
  const rawMaterialStore = useRawMaterialStore()
  const orderBillStore = useOrderBillStore()
  const orderBillDetails = ref(<CheckStock[]>[])

  const initialOrderBillDetail: OrderBillDetail = {
    quantity: 0,
    unitPrice: 0,
    totalPrice: 0,
    rawMaterial: rawMaterialStore.initialRawMaterial,
    orderBill: orderBillStore.initialOrderBill
  }

  const editedOrderBillDetail = ref<OrderBillDetail>(
    JSON.parse(JSON.stringify(initialOrderBillDetail))
  )

  const editedOrderBillDetails = ref<OrderBillDetail[]>([])

  async function getOrderBillDetails() {
    try {
      const res = await orderBillDetailService.getOrderBillDetails()
      orderBillDetails.value = res.data
    } catch (e: any) {
      console.log('error')
    }
  }

  async function saveOrderBillDetail() {
    try {
      const orderBillDetail = editedOrderBillDetail.value

      if (!orderBillDetail.id) {
        console.log(JSON.stringify(orderBillDetail))
        const res = await orderBillDetailService.addOrderBillDetail(orderBillDetail)
      }
      await getOrderBillDetails()
    } catch (e) {
      console.log(e)
    }
  }

  function clearForm() {
    editedOrderBillDetail.value = JSON.parse(JSON.stringify(initialOrderBillDetail))
  }

  return {
    getOrderBillDetails,
    clearForm,
    saveOrderBillDetail,
    editedOrderBillDetails,
    editedOrderBillDetail,
    initialOrderBillDetail
  }
})
