import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { CheckIn } from '@/types/CheckIn'
import checkInOutService from '@/services/checkInOut'
import { useAuthStore } from './auth'
import { useLoadingStore } from './loading'

export const useCheckInOutStore = defineStore('checkInOut', () => {
  const authStore = useAuthStore()
  authStore.getCurrentUser()
  const loadingStore = useLoadingStore()
  const checkInOutsAll = ref<CheckIn[]>([])
  const checkInOutsMe = ref<CheckIn[]>([])
  const checkInOutsBranch = ref<CheckIn[]>([])
  const initial: CheckIn = {
    checkIn: null!,
    checkOut: null!,
    status: '',
    user: null!,
    hour: 0
  }
  const userCheckInOut = ref<CheckIn>(JSON.parse(JSON.stringify(initial)))

  function formatDate(date: Date) {
    // const day = date.getDate().toString().padStart(2, '0')
    // const month = (date.getMonth() + 1).toString().padStart(2, '0')
    // const year = date.getFullYear()
    // console.log('GG ', `${day}:${month}:${year}`)
    return date.toLocaleDateString('th-TH', { hour12: false })
  }

  function formatTime(date: Date) {
    // const hours = date.getHours().toString().padStart(2, '0')
    // const min = date.getMinutes().toString().padStart(2, '0')
    // const sec = date.getSeconds().toString().padStart(2, '0')
    // console.log('GG ', `${hours}:${min}:${sec}`)
    return date.toLocaleTimeString('th-TH', { hour12: false })
  }

  // find all
  async function getCheckInOuts() {
    try {
      loadingStore.doLoad()
      const res = await checkInOutService.getCheckInOuts()
      //console.log(res.data)
      checkInOutsAll.value = res.data
      loadingStore.finishLoad()
    } catch (e) {
      loadingStore.finishLoad()
      console.log(e)
    }
  }

  // find by users id
  async function getCheckInOutMe() {
    try {
      loadingStore.doLoad()
      const userId = authStore.getCurrentUser()?.id
      if (userId) {
        const res = await checkInOutService.getCheckInOutMe(userId)
        //console.log(res.data)
        checkInOutsMe.value = res.data
      }
      loadingStore.finishLoad()
    } catch (e) {
      loadingStore.finishLoad()
      console.log(e)
    }
  }

  // find by branch
  async function getCheckInOutBranch() {
    try {
      loadingStore.doLoad()
      const branchId = authStore.getCurrentUser()?.branch?.id
      if (branchId) {
        const res = await checkInOutService.getCheckInOutBranch(branchId)
        //console.log(res.data)
        checkInOutsBranch.value = res.data
      }
      loadingStore.finishLoad()
    } catch (e) {
      loadingStore.finishLoad()
      console.log(e)
    }
  }

  async function checkIn() {
    try {
      loadingStore.doLoad()
      userCheckInOut.value.user = authStore.getCurrentUser()!
      const res = await checkInOutService.checkIn(userCheckInOut.value)
      console.log('store check in', res.data)
      showDate.value = userCheckInOut.value.checkIn.toLocaleDateString()
      showCheckIn.value = userCheckInOut.value.checkIn.toLocaleTimeString()
      userCheckInOut.value = res.data
      loadingStore.finishLoad()
    } catch (e) {
      loadingStore.finishLoad()
      console.log(e)
    }
  }

  async function checkOut() {
    try {
      loadingStore.doLoad()
      const res = await checkInOutService.checkOut(userCheckInOut.value)
      console.log('store check out', res.data)
      showCheckOut.value = userCheckInOut.value.checkOut.toLocaleTimeString()
      userCheckInOut.value = res.data
      loadingStore.finishLoad()
    } catch (e) {
      loadingStore.finishLoad()
      console.log(e)
    }
  }

  async function getCheckInOutByOne() {
    const res = await checkInOutService.getCheckInOutByOne(userCheckInOut.value.id!)
    userCheckInOut.value = res.data
  }

  function clear() {
    userCheckInOut.value = JSON.parse(JSON.stringify(initial))
  }
  const showDate = ref()
  const showCheckIn = ref()
  const showCheckOut = ref()

  return {
    checkInOutsAll,
    checkInOutsMe,
    userCheckInOut,
    showCheckOut,
    showDate,
    showCheckIn,
    checkInOutsBranch,
    clear,
    checkIn,
    checkOut,
    formatDate,
    formatTime,
    getCheckInOutMe,
    getCheckInOuts,
    getCheckInOutByOne,
    getCheckInOutBranch
  }
})
