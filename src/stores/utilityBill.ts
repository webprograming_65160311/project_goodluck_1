import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { UtilityBill } from '@/types/UtilityBill'
import UtilityBillService from '@/services/utilityBill'
import { useLoadingStore } from './loading'

const lastId = ref()

export const useUtilityBillStore = defineStore('utilityBill', () => {
  const utilityBills = ref(<UtilityBill[]>[])
  const loadingStore = useLoadingStore()
  const initialUtilityBill: UtilityBill = {
    invoiceNumber: '',
    date: null,
    amount: null,
    status: 'ยังไม่ได้จ่าย',
    user: null
  }
  const editedUtilityBill = ref<UtilityBill>(JSON.parse(JSON.stringify(initialUtilityBill)))
  const editedUtilityBills = ref<UtilityBill[]>([])

  async function saveUtilityBill() {
    try {
      const utilityBill = editedUtilityBill.value
      console.log('id ' + utilityBill.id)
      if (!utilityBill.id) {
        console.log('new')
        //add new
        const res = await UtilityBillService.addUtilityBill(utilityBill)
      }
    } catch (e) {
      console.log('errer')
    }
  }

  async function getUtilityBills() {
    loadingStore.doLoad()
    const res = await UtilityBillService.getUtilityBills()
    utilityBills.value = res.data
    console.log(utilityBills)
    loadingStore.finishLoad()
  }

  function clearForm() {
    editedUtilityBill.value = JSON.parse(JSON.stringify(initialUtilityBill))
  }

  return {
    saveUtilityBill,
    clearForm,
    getUtilityBills,
    initialUtilityBill,
    editedUtilityBills,
    editedUtilityBill,
    utilityBills,
    lastId
  }
})
