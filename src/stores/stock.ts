import type { Stock } from '@/types/Stock'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import stockService from '@/services/stock'

export const useStockStore = defineStore('stock', () => {
  const stocks = ref<Stock[]>([])

  const initialStock: Stock = {
    id: null,
    quantity: 0,
    price: 0,
    status: 'Normal',
    minimum: 0
  }
  const editedStock = ref<Stock>(JSON.parse(JSON.stringify(initialStock)))

  async function getStocks() {
    const res = await stockService.getstocks()
    stocks.value = res.data
  }

  return { stocks, getStocks }
})
