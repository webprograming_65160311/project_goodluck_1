import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { TypeUtilityBill } from '@/types/TypeUtilityBill'
import TypeUtilityBillService from '@/services/typeUtilityBill'
import { useLoadingStore } from './loading'

export const useTypeUtilityBillStore = defineStore('typeUtilityBill', () => {
  const typeUtilityBills = ref(<TypeUtilityBill[]>[])
  const loadingStore = useLoadingStore()
  const initialTypeUtilityBill: TypeUtilityBill = {
    name: ''
  }
  const editedTypeUtilityBill = ref<TypeUtilityBill>(
    JSON.parse(JSON.stringify(initialTypeUtilityBill))
  )
  const editedTypeUtilityBills = ref<TypeUtilityBill[]>([])

  async function saveTypeUtilityBill() {
    try {
      const typeUtilityBill = editedTypeUtilityBill.value
      console.log('id ' + typeUtilityBill.id)
      if (!typeUtilityBill.id) {
        console.log('new')
        //add new
        const res = await TypeUtilityBillService.addTypeUtilityBill(typeUtilityBill)
      }
    } catch (e) {
      console.log('errer')
    }
  }

  async function getTypeUtilityBills() {
    loadingStore.doLoad()
    const res = await TypeUtilityBillService.getTypeUtilityBills()
    typeUtilityBills.value = res.data
    loadingStore.finishLoad()
  }

  async function getTypeUtilityBill(id: number) {
    try {
      const res = await TypeUtilityBillService.getTypeUtilityBill(id)
      editedTypeUtilityBill.value = res.data
    } catch (e: any) {
      console.log('error')
    }
  }

  async function deleteType() {
    try {
      const TypeUtilityBill = editedTypeUtilityBill.value
      await TypeUtilityBillService.removeTypeUtilityBill(TypeUtilityBill)
      await getTypeUtilityBills()
    } catch (e: any) {
      console.log('error')
    }
  }

  function clearForm() {
    editedTypeUtilityBill.value = JSON.parse(JSON.stringify(initialTypeUtilityBill))
  }

  return {
    saveTypeUtilityBill,
    clearForm,
    getTypeUtilityBills,
    deleteType,
    getTypeUtilityBill,
    initialTypeUtilityBill,
    editedTypeUtilityBills,
    editedTypeUtilityBill,
    typeUtilityBills
  }
})
