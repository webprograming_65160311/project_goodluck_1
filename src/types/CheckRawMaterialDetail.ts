import type { CheckStock } from './CheckStock'
import type { RawMaterial } from './RawMaterial'

type CheckRawMaterialDetail = {
  id?: number
  bfCheck: number
  atCheck: number
  rawMaterial: RawMaterial
  checkStock: CheckStock
}

export { type CheckRawMaterialDetail }
