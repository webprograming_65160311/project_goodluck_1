import type { TypeUtilityBill } from './TypeUtilityBill'
import type { User } from './User'

type UtilityBill = {
  id?: number
  invoiceNumber: string
  date: Date | null
  amount: number | null
  remark?: string
  status: string
  user: User | null
  type?: TypeUtilityBill
}
export type { UtilityBill }
