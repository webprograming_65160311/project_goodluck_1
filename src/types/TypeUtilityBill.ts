type TypeUtilityBill = {
  id?: number
  name?: string
}

export type { TypeUtilityBill }
