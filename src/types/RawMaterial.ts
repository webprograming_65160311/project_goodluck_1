import type { Branch } from './Branch'
import type { Stock } from './Stock'

type Status = 'Normal' | 'Low Stock' | 'Out of Stock'

type RawMaterial = {
  id: number | null
  name: string
  // quantity: number | null
  // price: number | null
  // minimum?: number | null
  unit: string
  image?: string
  // status?: Status
  files: File[]
  branch: Branch[]
  stocks: Stock[]
}

export { type RawMaterial, type Status }
