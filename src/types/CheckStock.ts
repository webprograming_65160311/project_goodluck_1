import type { RawMaterial } from './RawMaterial'
import type { User } from './User'

type CheckStock = {
  id?: number
  checkDate: Date
  user: User | null
  rawMaterial?: RawMaterial
}

export { type CheckStock }
