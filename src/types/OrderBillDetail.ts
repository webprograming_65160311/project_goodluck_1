import type { OrderBill } from './OrderBill'
import type { RawMaterial } from './RawMaterial'

type OrderBillDetail = {
  id?: number
  quantity: number
  unitPrice: number
  totalPrice: number
  rawMaterial: RawMaterial
  orderBill: OrderBill
}

export type { OrderBillDetail }
