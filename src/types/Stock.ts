import type { Branch } from './Branch'
import type { RawMaterial } from './RawMaterial'

export type Stock = {
  id: number | null
  quantity: number | null
  price: number | null
  minimum?: number | null
  status?: string
  rawMaterial?: RawMaterial | null
  branch?: Branch | null
}
