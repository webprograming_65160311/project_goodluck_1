import type { User } from './User'

type CheckIn = {
  id?: number
  checkIn: Date
  checkOut: Date
  status: string //ทันเวลา  มาสาย
  hour?: number
  user?: User
}
export { type CheckIn }
