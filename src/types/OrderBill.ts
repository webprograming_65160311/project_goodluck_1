import type { User } from './User'

type OrderBill = {
  id?: number
  date: Date | null
  store: string
  totalPrice: number
  user: User | null
}

export type { OrderBill }
